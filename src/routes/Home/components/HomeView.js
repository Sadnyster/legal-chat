import React from 'react'
import './HomeView.scss'
import WizardForm from './WizardForm'

export const HomeView = () => (
  <div>
    <h1>Sign up to Talk with Lawyer</h1>
    <span>Welcome to Legal Chat, the easiest way to chat with Lawyer.</span>
    <WizardForm onSubmit={() => {}} />
  </div>
)

export default HomeView
