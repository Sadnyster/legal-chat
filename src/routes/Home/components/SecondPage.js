import React from 'react'
import { Field, reduxForm } from 'redux-form'
import validate from './validate'
import './HomeView.scss'

const renderError = ({ meta: { touched, error } }) =>
  touched && error ? <span>{error}</span> : false

const WizardFormSecondPage = props => {
  const { handleSubmit, previousPage } = props

  const styles = {
    float: 'left',
    'marginLeft': '125px',
    'textAlign': 'left'
  }
  return (
    <form onSubmit={handleSubmit} className='inputForm'>
      <div className='inputgroup'>
        <label>Please select Area Code</label>
        <div className='area-select'>
          <Field name='favoriteColor' component='select'>
            <option value=''>Area Code</option>
            <option value='205'>205</option>
            <option value='480'>480</option>
            <option value='209'>209</option>
          </Field>
        </div>
      </div>
      <div className='inputgroup'>
        <label>
          Available Numbers
        </label>
        <div style={styles}>
          <div style={{ 'margin-bottom': '10px' }}>(415) 800 3346</div>
          <div style={{ 'margin-bottom': '10px' }}>(415) 800 3346</div>
          <div style={{ 'margin-bottom': '10px' }}>(415) 800 3346</div>
          <div style={{ 'margin-bottom': '10px' }}>(415) 800 3346</div>
          <div style={{ 'margin-bottom': '10px' }}>
            (415) 800 3346
            &nbsp;&nbsp;
            <i className='fa fa-check' aria-hidden='true' style={{ 'color': '#1876F2' }} />
          </div>
          <div style={{ 'marginBottom': '10px' }}>(415) 800 3346</div>
        </div>
        <div className='clearFix' />
      </div>
      <div>
        <button type='submit' className='next'>Next</button>
      </div>
    </form>
  )
}

export default reduxForm({
  form: 'wizard', // Form name is same
  destroyOnUnmount: false,
  forceUnregisterOnUnmount: true, // <------ unregister fields on unmount
  validate
})(WizardFormSecondPage)
