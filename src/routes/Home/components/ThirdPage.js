import React from 'react'
import { Field, reduxForm } from 'redux-form'
import validate from './validate'
import renderField from './renderField'
import './HomeView.scss'

const renderColorSelector = ({ input, meta: { touched, error } }) => (
  <div>
    <select {...input}>
      <option value=''>Select a color...</option>
      {colors.map(val => <option value={val} key={val}>{val}</option>)}
    </select>
    {touched && error && <span>{error}</span>}
  </div>
)

const WizardFormThirdPage = props => {
  const { handleSubmit, pristine, previousPage, submitting } = props
  return (
    <form onSubmit={handleSubmit} className='inputForm'>
      <div className='card-info'>
        <div className='cardNumber'>
          <Field
            name='cardNumber'
            type='text'
            label='4242 4242 4242'
            component={renderField}
          />
        </div>
        <div className='goodThrough'>
          <Field
            name='goodThrough'
            type='text'
            label='MM/YY'
            component={renderField}
          />
        </div>
        <div className='cvc'>
          <Field
            name='cvc'
            type='text'
            label='CVC'
            component={renderField}
          />
        </div>
      </div>
      <div>
        <button type='submit' className='next'>Save Card</button>
      </div>
    </form>
  )
}
export default reduxForm({
  form: 'wizard', // Form name is same
  destroyOnUnmount: false,
  forceUnregisterOnUnmount: true, // <------ unregister fields on unmount
  validate
})(WizardFormThirdPage)

