import React, { Component } from 'react'
import PropTypes from 'prop-types'

import FirstPage from './FirstPage'
import SecondPage from './SecondPage'
import ThirdPage from './ThirdPage'

class WizardForm extends Component {
  constructor (props) {
    super(props)
    this.nextPage = this.nextPage.bind(this)
    this.previousPage = this.previousPage.bind(this)
    this.goToStep = this.goToStep.bind(this)
    this.state = {
      page: 1
    }
  }
  goToStep (step) {
    this.setState({ page: step })
  }

  nextPage () {
    this.setState({ page: this.state.page + 1 })
  }

  previousPage () {
    this.setState({ page: this.state.page - 1 })
  }

  render () {
    const { onSubmit } = this.props
    const { page } = this.state
    return (
      <div>
        <div className='stepSection'>
          <ul>
            <li className={this.state.page === 1 ? 'active' : ''} onClick={() => this.goToStep(1)} >
              <div className='stepNumber'>1</div> Account
            </li>
            <li className={this.state.page === 2 ? 'active' : ''} onClick={() => this.goToStep(2)}>
              <div className='stepNumber'>2</div> Phone Number
            </li>
            <li className={this.state.page === 3 ? 'active' : ''} onClick={() => this.goToStep(3)}>
              <div className='stepNumber'>3</div> Payment
            </li>
          </ul>
        </div>
        {page === 1 && <FirstPage onSubmit={this.nextPage} />}
        {page === 2 &&
          <SecondPage
            previousPage={this.previousPage}
            onSubmit={this.nextPage}
          />}
        {page === 3 &&
          <ThirdPage
            previousPage={this.previousPage}
            onSubmit={onSubmit}
          />}
      </div>
    )
  }
}

WizardForm.propTypes = {
  onSubmit: PropTypes.func.isRequired
}

export default WizardForm
