import React from 'react'
import { Field, reduxForm } from 'redux-form'
import validate from './validate'
import renderField from './renderField'

const WizardFormFirstPage = props => {
  const { handleSubmit } = props
  return (
    <form onSubmit={handleSubmit} className='inputForm'>
      <Field
        name='email'
        type='text'
        label='Email'
        component={renderField}
      />
      <Field
        name='password'
        type='password'
        component={renderField}
        label='Password'
      />
      <Field
        name='fullName'
        type='text'
        component={renderField}
        label='Fullname'
      />
      <Field
        name='phoneNumber'
        type='phone'
        component={renderField}
        label='Phone Number'
      />

      <div className='term-text'>
        By continue, you agree to our <a href=''>Terms</a>
        &nbsp;and that you have read our <a href=''>Data Use Policy</a>, including our Cookie Use.
      </div>
      <div>
        <button type='submit' className='next'>Next</button>
      </div>
    </form>
  )
}

export default reduxForm({
  form: 'wizard', // <------ same form name
  destroyOnUnmount: false, // <------ preserve form data
  forceUnregisterOnUnmount: true, // <------ unregister fields on unmount
  validate
})(WizardFormFirstPage)
