import React from 'react'

const renderField = ({ input, className, label, type, meta: { touched, error }}) => (
  <div className='inputgroup'>
    <div>
      <input {...input} className={className} placeholder={label} type={type} />
    </div>
  </div>
)

export default renderField
