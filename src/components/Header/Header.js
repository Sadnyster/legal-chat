import React from 'react'
import './Header.scss'
import logo from './assets/logo.png'

export const Header = () => (
  <div className='header'>
    <img src={logo} /> <span>Legal</span> Chat
  </div>
)

export default Header
